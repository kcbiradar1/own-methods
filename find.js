function find(elements, callback) {
  if (Array.isArray(elements)) {
    for (let index = 0; index < elements.length; index++) {
      if (callback(elements[index])) {
        return true;
      }
    }
  } else {
    return false;
  }
}

module.exports = find;
