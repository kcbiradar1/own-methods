const items = [1, 2, 3, 4, 5, 5];

const reduce = require("./reduce");

let startingValue;

function callback(startingValue, currentValue) {
  if (startingValue === undefined) {
    startingValue = currentValue;
  } else {
    startingValue += currentValue;
  }
  return startingValue;
}

const results = reduce(items, callback, startingValue);

console.log(results);
