const items = [1, 2, 3, 4, 5, 5];

function callback(elements, index) {
  elements[index] += 2;
}

const map = require("./map");

map(items, callback);

console.log(items);
