const items = [1, 2, 3, 4, 5, 5];

function callback(element) {
  if (element & 1) return true;
}

const filter = require("./filter");

console.log(filter(items, callback));
