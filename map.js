function map(elements, callback) {
  if (Array.isArray(elements)) {
    for (let index = 0; index < elements.length; index++) {
      callback(elements, index);
    }
  } else {
    return [];
  }
}

module.exports = map;
