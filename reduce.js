function reduce(elements, callback, startingValue) {
  if (Array.isArray(elements)) {
    for (let index = 0; index < elements.length; index++) {
      startingValue = callback(startingValue, elements[index]);
    }
    return startingValue;
  } else {
    return Infinity;
  }
}

module.exports = reduce;
