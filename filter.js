function filter(elements, callback) {
  if (Array.isArray(elements)) {
    let result_array = [];
    for (let index = 0; index < elements.length; index++) {
      if (callback(elements[index])) {
        result_array.push(elements[index]);
      }
    }
    return result_array;
  } else {
    return [];
  }
}

module.exports = filter;
