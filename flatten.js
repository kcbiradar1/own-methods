let result_array = [];
function flatten(elements) {
  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index])) {
      flatten(elements[index]);
    } else {
      result_array.push(elements[index]);
    }
  }
  return result_array;
}

module.exports = flatten;
